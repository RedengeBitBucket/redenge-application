![Redenge logo](http://redenge.cz/wp-content/uploads/LOGO-REDENGE-internet-solutions.png)
# Redenge aplikace
Obsahuje základní objekty pro základ každého e-commerce řešení.

## Požadavky
- verze PHP 5.6 a vyšší
- balíček nette/nette

## Instalace
Nejlepší možnost, jak nainstalovat balíček je přes composer  [Composer](http://getcomposer.org/):
1) Do svého composer.json přidejte odkaz na privátní repozitář satis
```sh
"repositories": [
        {
            "type": "composer",
            "url": "https://satis.redenge.biz"
        }
]
```
2) Stáhněte balíček:
```sh
$ composer require redenge/application
```

3) Do config.local.neon aplikace přidat požadovanou cache storage:
```twig
services:
	appCacheStorage:
		class: Nette\Caching\Storages\MemcachedStorage() # Nette\Caching\Storages\NewMemcachedStorage()
		autowired: no
```

4) Do config.neon přidat extension:
```twig
extensions:
	redengeApplication: Redenge\Application\DI\RedengeApplicationExtension
```

5) Do config.neon aplikace přidat strategii detekce prostředí:
```twig
services:
	environmentDetectionStrategy:
		class: Redenge\Application\Environment\ShopEnvironmentDetectionStrategy()
		autowired: no
```
Dostupné strategie:

Class                                                              | Popis
------------------------------------------------------------------ | -------------
Redenge\Application\Environment\ShopEnvironmentDetectionStrategy() | Využití u starších redenge projektů, které využívají třídu ,,shop" děděnou od ,,cinterface". Je nutné, aby třída ,,shop" byla zaregistrovaná v config.neon jako služba. Jako příklad může posloužit projekt ,,GUMEX" |

## Aktuálně
- obsahuje službu pro detekci prostředí
