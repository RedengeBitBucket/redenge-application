<?php

namespace Redenge\Application\Entity;

use Nette\Utils\Strings;


abstract class Entity
{

	/**
	 * @var int
	 */
	private $id;


	/**
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = (int) $id;
	}


	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}


	/**
	 * Načtení vlastností třídy pomocí pole
	 *
	 * @param array $array
	 */
	public function loadFromArray(array $array)
	{
		$columns = array_keys($array);
		foreach ($columns as $columnOrig) {
			$column = $this->toCamel($columnOrig);
			$column = Strings::firstUpper($column);
			$functionName = 'set' . $column;
			if (method_exists($this, $functionName)) {
				call_user_func_array([$this, $functionName], [$array[$columnOrig]]);
			}
		}
	}


	/**
	 * Převedení řetězce do camelcase
	 *
	 * @param string $string
	 * @return string
	 */
	private function toCamel($string)
	{
		$string = ucwords($string, "_");
		$string = str_replace('_', '', $string);
		return $string;
	}


	public function getArray()
	{
		return get_object_vars($this);
	}

}
