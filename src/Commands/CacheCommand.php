<?php

namespace Redenge\Application\Commands;

use Nette\Utils\Finder;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class CacheCommand extends Command
{

	protected function configure()
	{
		$this->setName('cache:remove')
			->setDescription('Application cache remove');
	}


	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$cachePath = APP_DIR . '/temp/cache';
		foreach (Finder::findFiles('*')->exclude('journal.*')->from($cachePath) as $file) {
			unlink($file);
		}

		$output->writeln('<info>Cache is deleted</info>');
		return 1;
	}

}
