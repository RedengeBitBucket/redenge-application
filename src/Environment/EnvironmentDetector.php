<?php

namespace Redenge\Application\Environment;

use Nette\Caching\Cache;
use Nette\Caching\IStorage;
use Nette\Http\Request;


/**
 * Description of EnvironmentDetector
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class EnvironmentDetector
{

	const CACHE_NS = 'redenge.application.environment.environmentDetector',
		CACHE_EXPIRE = '30 minutes',
		CACHE_TAG_DETECTION = ['environmentDetection'];

	/**
	 * @var Environment
	 */
	private $environment;

	/**
	 * @var Request
	 */
	private $request;

	/**
	 * @var IEnvironmentDetectionStategy
	 */
	private $strategy;

	/**
	 * @var Cache
	 */
	private $cache;


	public function __construct(
		Environment $environment,
		Request $request,
		IEnvironmentDetectionStategy $strategy,
		IStorage $storage
	) {
		$this->environment = $environment;
		$this->request = $request;
		$this->strategy = $strategy;
		$this->cache = new Cache($storage, self::CACHE_NS);
		$this->detect();
	}


	private function detect()
	{
		$host = $this->request->getUrl()->getHost();
		$country = $this->request->getQuery('country', '');
		if ($country === '') {
			/*
			 * zkusí načíst zemi z URL path (www.domena.cz/en/)
			 */
			$path = $this->request->getUrl()->getPath();
			if (preg_match('~^\/(\w{2})\/~', $path, $matches)) {
				$country = $matches[1];
			}
		}

		$environment = $this->performDetectionFromCache($host, $country);
		$this->strategy->loadEnvironment($environment, $this->environment);
	}


	/**
	 * @param string $host
	 * @param string $country
	 *
	 * @return EnvironmentDetectorResult
	 */
	public function performDetectionFromCache($host, $country)
	{
		$key = [$host, $country];

		/** @var EnvironmentDetectorResult $result */
		$result = $this->cache->load($key);
		if ($result === NULL) {
			$result = $this->cache->save($key, function () {
				return $this->strategy->detect();
			}, [Cache::EXPIRE => self::CACHE_EXPIRE, Cache::TAGS => self::CACHE_TAG_DETECTION]);
		}

		return $result;
	}

}
