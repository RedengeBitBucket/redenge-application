<?php

namespace Redenge\Application\Environment;


/**
 * Description of EnvironmentDetectorResult
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 *
 * @property array $domain
 * @property array $multishop
 * @property array $multishopAttribute
 * @property array $multishopAnalytics
 * @property array $settingsProfile
 * @property array $settingsProfileSecondaryCurrency
 * @property array $country
 * @property array $currency
 * @property array $language
 * @property bool $needCountry
 */
class EnvironmentDetectorResult
{

	use \Nette\SmartObject;

	/**
	 * @var array
	 */
	private $domain;

	/**
	 * @var array
	 */
	private $multishop;

	/**
	 * @var array
	 */
	private $multishopAttribute;

	/**
	 * @var array
	 */
	private $multishopAnalytics;

	/**
	 * @var array
	 */
	private $settingsProfile;

	/**
	 * @var array
	 */
	private $settingsProfileSecondaryCurrency;

	/**
	 * @var array
	 */
	private $country;

	/**
	 * @var array
	 */
	private $currency;

	/**
	 * @var array
	 */
	private $language;

	/**
	 * @var bool
	 */
	private $needCountry;


	/**
	 * @return array
	 */
	public function getDomain()
	{
		return $this->domain;
	}


	/**
	 * @return array
	 */
	public function getMultishop()
	{
		return $this->multishop;
	}


	/**
	 * @return array
	 */
	public function getMultishopAttribute()
	{
		return $this->multishopAttribute;
	}


	/**
	 * @return array
	 */
	public function getMultishopAnalytics()
	{
		return $this->multishopAnalytics;
	}


	/**
	 * @return array
	 */
	public function getSettingsProfile()
	{
		return $this->settingsProfile;
	}


	/**
	 * @return array
	 */
	public function getSettingsProfileSecondaryCurrency()
	{
		return $this->settingsProfileSecondaryCurrency;
	}


	/**
	 * @return array
	 */
	public function getCountry()
	{
		return $this->country;
	}


	/**
	 * @return array
	 */
	public function getCurrency()
	{
		return $this->currency;
	}


	/**
	 * @return array
	 */
	public function getLanguage()
	{
		return $this->language;
	}


	/**
	 * @return bool
	 */
	public function getNeedCountry()
	{
		return $this->needCountry;
	}


	/**
	 * @param array $domain
	 * @return \Redenge\Application\Environment\EnvironmentDetectorResult
	 */
	public function setDomain($domain)
	{
		$this->domain = $domain;
		return $this;
	}


	/**
	 * @param array $multishop
	 * @return \Redenge\Application\Environment\EnvironmentDetectorResult
	 */
	public function setMultishop($multishop)
	{
		$this->multishop = $multishop;
		return $this;
	}


	/**
	 * @param array $multishop
	 * @return \Redenge\Application\Environment\EnvironmentDetectorResult
	 */
	public function setMultishopAttribute($multishopAttribute)
	{
		$this->multishopAttribute = $multishopAttribute;
		return $this;
	}


	/**
	 * @param array $multishop
	 * @return \Redenge\Application\Environment\EnvironmentDetectorResult
	 */
	public function setMultishopAnalytics($multishopAnalytics)
	{
		$this->multishopAnalytics = $multishopAnalytics;
		return $this;
	}


	/**
	 * @param array $multishop
	 * @return \Redenge\Application\Environment\EnvironmentDetectorResult
	 */
	public function setSettingsProfile($settingsProfile)
	{
		$this->settingsProfile = $settingsProfile;
		return $this;
	}


	/**
	 * @param array $multishop
	 * @return \Redenge\Application\Environment\EnvironmentDetectorResult
	 */
	public function setSettingsProfileSecondaryCurrency($settingsProfileSecondaryCurrency)
	{
		$this->settingsProfileSecondaryCurrency = $settingsProfileSecondaryCurrency;
		return $this;
	}


	/**
	 * @param array $multishop
	 * @return \Redenge\Application\Environment\EnvironmentDetectorResult
	 */
	public function setCountry($country)
	{
		$this->country = $country;
		return $this;
	}


	/**
	 * @param array $multishop
	 * @return \Redenge\Application\Environment\EnvironmentDetectorResult
	 */
	public function setCurrency($currency)
	{
		$this->currency = $currency;
		return $this;
	}


	/**
	 * @param array $multishop
	 * @return \Redenge\Application\Environment\EnvironmentDetectorResult
	 */
	public function setLanguage($language)
	{
		$this->language = $language;
		return $this;
	}


	/**
	 * @param bool $needCountry
	 * @return \Redenge\Application\Environment\EnvironmentDetectorResult
	 */
	public function setNeedCountry($needCountry)
	{
		$this->needCountry = $needCountry;
		return $this;
	}

}
