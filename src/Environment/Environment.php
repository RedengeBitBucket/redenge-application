<?php

namespace Redenge\Application\Environment;


/**
 * Description of Environment
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 *
 * @property string $multishopCode
 * @property string $profileCode
 * @property string $countryIso
 * @property string $languageIso
 * @property int $countryId
 */
class Environment
{

	use \Nette\SmartObject;

	/**
	 * @var string
	 */
	private $multishopCode;

	/**
	 * @var string
	 */
	private $profileCode;

	/**
	 * @var string
	 */
	private $countryIso;

	/**
	 * ISO 639/1
	 *
	 * @var string
	 */
	private $languageIso;

	/**
	 * @var int
	 */
	private $countryId;


	/**
	 * @return string
	 */
	public function getMultishopCode()
	{
		return $this->multishopCode;
	}


	/**
	 * @return string
	 */
	public function getProfileCode()
	{
		return $this->profileCode;
	}


	/**
	 * @return string
	 */
	public function getCountryIso()
	{
		return $this->countryIso;
	}


	/**
	 * @return string
	 */
	public function getLanguageIso()
	{
		return $this->languageIso;
	}


	/**
	 * @return int
	 */
	public function getCountryId()
	{
		return $this->countryId;
	}


	/**
	 * @param string $multishopCode
	 * @return \Redenge\Application\Environment\Environment
	 */
	public function setMultishopCode($multishopCode)
	{
		$this->multishopCode = $multishopCode;
		return $this;
	}


	/**
	 * @param string $profileCode
	 * @return \Redenge\Application\Environment\Environment
	 */
	public function setProfileCode($profileCode)
	{
		$this->profileCode = $profileCode;
		return $this;
	}


	/**
	 * @param string $countryIso
	 * @return \Redenge\Application\Environment\Environment
	 */
	public function setCountryIso($countryIso)
	{
		$this->countryIso = $countryIso;
		return $this;
	}


	/**
	 * @param string $languageIso
	 * @return \Redenge\Application\Environment\Environment
	 */
	public function setLanguageIso($languageIso)
	{
		if (strlen($languageIso) !== 2) {
			throw new \InvalidArgumentException('Language code is in bad format.'
				. " The correct format is according to ISO 639/1. You trying set ,,$languageIso''");
		}
		if ($languageIso === 'cz') {
			$languageIso = 'cs';
		}
		$this->languageIso = $languageIso;
		return $this;
	}


	/**
	 * @param int $countryId
	 * @return \Redenge\Application\Environment\Environment
	 */
	public function setCountryId($countryId)
	{
		$this->countryId = $countryId;
		return $this;
	}


	public function loadByShop(\shop $shop)
	{
		$this->multishopCode = $shop->multishop_interface->multishop->code;
		$this->profileCode = $shop->settings_interface->profile->code;
		$this->countryIso = $shop->settings_interface->country->code;
		$this->setLanguageIso($shop->settings_interface->language->code);
	}

}
