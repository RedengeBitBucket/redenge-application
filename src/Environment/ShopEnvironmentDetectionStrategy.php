<?php

namespace Redenge\Application\Environment;

use Nette\Http\Request;
use shop;


/**
 * Detekce prostředí u projektů, které využívají třídu ,,shop" děděnou od ,,cinterface"
 * Např. ,,multishop_interface, settings_interface, ..."
 *
 * Vychází z projektu Gumex. Pokud se zjistí, že je to na míru pouze pro Gumex, tak by tato strategie měla být jen
 * v projektu gumex a ne v obecném projektu aplikace
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
final class ShopEnvironmentDetectionStrategy implements IEnvironmentDetectionStategy
{

	/**
	 * @var shop
	 */
	protected $shop;

	/**
	 * @var Request
	 */
	protected $request;


	public function __construct(shop $shop, Request $request)
	{
		$this->shop = $shop;
		$this->request = $request;
	}


	/**
	 * {@inheritdoc}
	 */
	public function detect()
	{
		$multishopId = $this->request->getQuery('ms', 0);

		$this->shop->detect_settings($multishopId);

		return (new EnvironmentDetectorResult)
			->setDomain($this->shop->settings_interface->domain->getPairs())
			->setMultishop($this->shop->multishop_interface->multishop->getPairs())
			->setMultishopAttribute($this->shop->multishop_interface->multishop->attribute->getPairs())
			->setMultishopAnalytics($this->shop->multishop_interface->multishop->analytics->getPairs())
			->setSettingsProfile($this->shop->settings_interface->profile->getPairs())
			->setSettingsProfileSecondaryCurrency($this->shop->settings_interface->profile->secondary_currency->getPairs())
			->setCountry($this->shop->settings_interface->country->getPairs())
			->setCurrency($this->shop->settings_interface->currency->getPairs())
			->setLanguage($this->shop->settings_interface->language->getPairs())
			->setNeedCountry($this->shop->need_country());
	}


	public function loadEnvironment(EnvironmentDetectorResult $environmentResult, Environment $environment)
	{
		$this->shop->settings_interface->domain->loadFromArray($environmentResult->domain);
		$this->shop->multishop_interface->multishop->loadFromArray($environmentResult->multishop);
		$this->shop->multishop_interface->multishop->attribute->loadFromArray($environmentResult->multishopAttribute);
		$this->shop->multishop_interface->multishop->analytics->loadFromArray($environmentResult->multishopAnalytics);
		$this->shop->settings_interface->country->loadFromArray($environmentResult->country);
		$this->shop->settings_interface->currency->loadFromArray($environmentResult->currency);
		$this->shop->settings_interface->language->loadFromArray($environmentResult->language);
		$this->shop->settings_interface->profile->loadFromArray($environmentResult->settingsProfile);
		$this->shop->settings_interface->profile->secondary_currency->loadFromArray(
			$environmentResult->settingsProfileSecondaryCurrency);

		$this->shop->setNeedCountry($environmentResult->needCountry);

		$_SESSION['country'] = $this->shop->settings_interface->country->code;
		$_SESSION['language'] = $this->shop->settings_interface->language->code;
		$_SESSION['currency'] = $this->shop->settings_interface->currency->code;

		$environment->loadByShop($this->shop);
	}

}
