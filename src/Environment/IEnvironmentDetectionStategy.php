<?php

namespace Redenge\Application\Environment;


/**
 * Description of IEnvironmentDetectionStategy
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
interface IEnvironmentDetectionStategy
{

	/**
	 * @return EnvironmentDetectorResult
	 */
	function detect();


	function loadEnvironment(EnvironmentDetectorResult $environmentResult, Environment $environment);

}
