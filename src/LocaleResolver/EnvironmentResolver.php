<?php

namespace Redenge\Application\LocaleResolver;

use Kdyby\Translation\IUserLocaleResolver;
use Kdyby\Translation\Translator;
use Redenge\Application\Environment\Environment;


/**
 * Description of EnvironmentResolver
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class EnvironmentResolver implements IUserLocaleResolver
{

	/**
	 * @var Environment
	 */
	private $environment;


	public function __construct(Environment $environment)
	{
		$this->environment = $environment;
	}


	public function resolve(Translator $translator)
	{
		return $this->environment->languageIso;
	}

}
