<?php

namespace Redenge\Application\Components\Controls;

use Nette\Forms\Controls\BaseControl;
use Nette\Forms\Container;


/**
 * Description of Html
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class Html extends BaseControl
{

	const CALL_REGISTER_METHOD = __CLASS__ . '::register();';

	/**
	 * @var string
	 */
	private $html;


	/**
	 * @param string $html
	 * @return \Redenge\Application\Components\Controls\Html
	 */
	public function setHtml($html)
	{
		$this->html = $html;
		return $this;
	}


	public function getControl()
	{
		return $this->html;
	}


	public static function register()
	{
		Container::extensionMethod('addHtml', function (Container $container, $name, $label = NULL) {
			return $container[$name] = new Html($label);
		});
	}

}
