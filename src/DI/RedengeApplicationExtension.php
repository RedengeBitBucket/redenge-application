<?php

namespace Redenge\Application\DI;

use Nette\DI\CompilerExtension;
use Nette\PhpGenerator\ClassType;
use Redenge\Application\Components\Controls\Html;


/**
 * Description of RedengeApplicationExtension
 *
 * @author Bc. Michal Smejkal <m.smejkal123@gmail.com>
 */
class RedengeApplicationExtension extends CompilerExtension
{

	/**
	 * {@inheritdoc}
	 */
	public function loadConfiguration()
	{
		parent::loadConfiguration();

		$builder = $this->getContainerBuilder();

		$builder->addDefinition($this->prefix('env'))
			->setClass('Redenge\Application\Environment\Environment');

		$builder->addDefinition($this->prefix('envDetector'))
			->setClass('Redenge\Application\Environment\EnvironmentDetector', [
				'strategy' => '@environmentDetectionStrategy',
				'storage' => '@appCacheStorage',
			])
			->setTags(['run' => true]);
	}


	/**
	 * {@inheritdoc}
	 */
	public function afterCompile(ClassType $class)
	{
		$initialize = $class->getMethod('initialize');
		$initialize->addBody(Html::CALL_REGISTER_METHOD);
	}

}
